*****************************SPI BRANCH**********************************
* Initializations
*************************************************************************

*BASE	EQU	$1000
LCDBAS	EQU	$B5F0
PORTD	EQU	$1008
DDRD	EQU	$1009
TCNT	EQU	$100E
TOC2	EQU	$1018
TMSK1	EQU	$1022
TFLG1	EQU	$1023
OPTION	EQU	$1039
ADCTL	EQU	$1030
ADR4	EQU	$1034
TW_MS	EQU	40000	; Clock cycles for 20 ms
HALF_S	EQU	25	; At 20 ms, # of counts until 1/2 sec
ONE_S	EQU	50	; At 20 ms, # of counts until 1 sec
GRN	EQU	$10	; Port D pin 4 attached to greed LED
RED	EQU	$08	; Port D pin 3 attached to red LED
RG	EQU	$18	; Special condition where both LEDs should flash
OFF	EQU	$00	; LEDs should be off for certain ranges
USP	EQU	$E6	; Upper setpoint - 4.5 V
SP1	EQU	$CD	; Setpoint 1 - 4 V
SP2	EQU	$B3	; Setpoint 2 - 3.5 V
SP3	EQU	$66	; Setpoint 3 - 2 V
SP4	EQU	$4D	; Setpoint 4 - 1.5 V
LSP	EQU	$33	; Lower setpoint - 1 V
CHRBASE	EQU	$30	; Reference point for converting binary to ASCII

***********************************************
* For EEPROM:
*
	ORG	$0200
*--------------------------
*
* For RAM:
*
*	ORG	$2000
*--------------------------
*
* For THRSim:
*
*	ORG	$0000
***********************************************

CVAL	RMB	2	; 2 bytes for the converted value
RMNDR	RMB	2	; 2 bytes for the tenths place
RMNDR2	RMB	2	; 2 bytes for the hundredths place
ALM	RMB	1	; 1 byte for LED flashing condition
RATE	RMB	1	; 1 byte for LED flashing rate
CNT	RMB	1	; 1 byte for counting to rate (1 sec or 1/2 sec)
CHAR1	RMB	1	; 1 byte for the char representation of the converted value
DEC	FCC	'.'	; Decimal point
CHAR2	RMB	1	; 1 byte for the char repr of the tenths place
CHAR3	RMB	1	; 1 byte for the char repr of the hundredths place
	FCB	0	; Terminator for voltage reading
GROUP	FCC	'Group 9'
	FCB	0
NAMES	FCC	'CW EE MM MP'
	FCB	0
MSG	FCC	'Voltage: '
	FCB	0

*************************************************************************
* Main program
*************************************************************************

***********************************************
* For EEPROM:
*
*	ORG	$FFFE
*	FDB	MAIN
*
*	ORG	$E000
*MAIN	LDS	#$23FF
*--------------------------
*
* For RAM:
*
	ORG	$1040
START	LDS	#$23FF
*--------------------------
*
* For THRSim:
*
*	ORG	$C000
*START	LDS	#$01FF
***********************************************

MAIN	SEI
	JSR	LCDSET

******Switching on A/D system and waiting for it to stablize******

	LDAA	#$80
	STAA	OPTION
	LDY	#30
WAIT	DEY
	BNE	WAIT

******Configuration for scanning on a single channel******

	LDAA	#$27
	STAA	ADCTL

******Additional preparation******

	CLR	CNT
	LDAA	#$18
	STAA	DDRD
	LDD	TCNT
	ADDD	#TW_MS
	STD	TOC2
	LDAA	#$40
	STAA	TMSK1
	CLI

******Print preliminary messages******

	LDX	#GROUP
	JSR	PRINT
	JSR	ROW2
	LDX	#NAMES
	JSR	PRINT

******Main routine******

READ	JSR	ROW3
	LDX	#MSG
	JSR	PRINT
	LDAA	ADR4
	JSR	SETALM
	JSR	CONV
	JSR	CHRCONV
	LDX	#CHAR1
	JSR	PRINT
	BRA	READ

*************************************************************************
* Set alarm state subroutine (decides active LEDs and flashing rates)
*************************************************************************

SETALM	CMPA	#SP1
	BLO	SETPT2
	LDAB	#GRN
	BRA	R_SET
SETPT2	CMPA	#SP2
	BHI	VOID
	CMPA	#SP3
	BLO	SETPT4
	LDAB	#RG
	BRA	R_SET
SETPT4	CMPA	#SP4
	BHI	VOID	
	LDAB	#RED
	BRA	R_SET
VOID	LDAB	#OFF
R_SET	STAB	ALM
	CMPA	#USP
	BHS	ONE
	CMPA	#LSP
	BLS	ONE
	LDAB	#HALF_S
	BRA	DONE
ONE	LDAB	#ONE_S
DONE	STAB	RATE
	RTS

*************************************************************************
* Digital-to-Analog conversion subroutine
*************************************************************************

CONV	LDAB	#5
	MUL
	LDX	#255
	IDIV
	STX	CVAL
	STD	RMNDR
	LDAA	RMNDR+1
	LDAB	#10
	MUL
	LDX	#255
	IDIV
	STX	RMNDR
	STD	RMNDR2
	LDAA	RMNDR2+1
	LDAB	#10
	MUL
	LDX	#255
	IDIV	
	STX	RMNDR2
	RTS

*************************************************************************
* Binary to ASCII subroutine
*************************************************************************

CHRCONV	LDAA	#CHRBASE
	ADDA	CVAL+1
	STAA	CHAR1
	LDAA	#CHRBASE
	ADDA	RMNDR+1
	STAA	CHAR2
	LDAA	#CHRBASE
	ADDA	RMNDR2+1
	STAA	CHAR3
	RTS
	
*************************************************************************
* LCD initialization subroutine
*************************************************************************

LCDSET LDAA  #$3C
       STAA  LCDBAS
       JSR   DELAY
       LDAA  #$01
       STAA  LCDBAS
       JSR   DELAY
       LDAA  #$0F
       STAA  LCDBAS
       JSR   DELAY
       LDAA  #$06
       STAA  LCDBAS
       LDAA  #$14
       STAA  LCDBAS
       LDAA  #$02
       STAA  LCDBAS
       JSR   DELAY
       RTS

*************************************************************************
* Printing subroutine
*************************************************************************

PRINT	PSHA
PR01	LDAA	0,X		* Get a character from the message
	BEQ	PRDONE		* If end of message then done
	JSR	LCDOUT		* Else send the character to the LCD		
	INX
	BRA	PR01
PRDONE	PULA
	RTS

*************************************************************************
* Move cursor to row 1 - subroutine
*************************************************************************

ROW1	PSHA
	LDAA	#$02
	STAA	LCDBAS
	JSR	DELAY1
	PULA
	RTS

*************************************************************************
* Move cursor to row 2 - subroutine
*************************************************************************

ROW2	PSHA
	LDAA	#$A8
	STAA	LCDBAS
	JSR	DELAY1
	PULA
	RTS

*************************************************************************
* Move cursor to row 3 - subroutine
*************************************************************************

ROW3	PSHA
	LDAA	#$94
	STAA	LCDBAS
	JSR	DELAY1
	PULA
	RTS

*************************************************************************
* Move cursor to row 4 - subroutine
*************************************************************************

ROW4	PSHA
	LDAA	#$D4
	STAA	LCDBAS
	JSR	DELAY1
	PULA
	RTS

*************************************************************************
* Delay subroutine
*************************************************************************

DELAY  PSHB 		* save registers
       PSHA
       LDAB  #$40	* set loop count 1
DEL1   LDAA  #$F0	* set loop count 2
DEL2   DECA             * decrement counter 2
       BNE   DEL2       * branch if not 0
       DECB		* decrement counter 1
       BNE   DEL1	* branch if not 0
       PULA		* restore registers
       PULB
       RTS
*
*  
DELAY1	PSHB 		* save registers
	PSHA
	LDAB  #$01	* set loop count 1
DEL11   LDAA  #$F0	* set loop count 2
DEL12   DECA            * decrement counter 2
	BNE   DEL12     * branch if not 0
	DECB		* decrement counter 1
	BNE   DEL11	* branch if not 0
	PULA		* restore registers
	PULB
	RTS

*************************************************************************
* Out to LCD routine
*************************************************************************

LCDOUT
	LDAB	LCDBAS		* get current address
	CMPB 	#$13		* at end of first row
	BEQ     lcd_ad1		* jif yes
	CMPB 	#$53		* at end of 2nd row
	BEQ     lcd_ad2		* jif yes
	CMPB 	#$27		* at end of 3rd row
	BEQ     lcd_ad3		* jif yes

	STAA	LCDBAS+1    * write data to lcd output port
        JSR	DELAY1
	RTS

lcd_ad1
	LDAB	#$28+$80	* set correct address
	BRA	lcd_fix		* continue fix
lcd_ad2
	LDAB	#$14+$80	* set correct address
	BRA	lcd_fix		* continue fix
lcd_ad3
	LDAB	#$54+$80	* set correct address
lcd_fix
	STAA	LCDBAS+1    * write data to lcd output port
	JSR	DELAY1
	STAB	LCDBAS		* make address fix
	JSR	DELAY1
	RTS

*************************************************************************
* Interrupt service routine
*************************************************************************

SVC	LDD	TOC2
	ADDD	#TW_MS
	STD	TOC2
	INC	CNT
	LDAA	RATE
	CMPA	CNT
	BNE	EXIT
	CLR	CNT
	LDAB	PORTD
	ANDB	#$18
	CMPB	ALM
	BNE	NEW
	EORB	ALM
	STAB	PORTD
	BRA	EXIT
NEW	CLR	PORTD
	LDAB	ALM
	STAB	PORTD
EXIT	LDAA	#$40
	STAA	TFLG1
	RTI

*************************************************************************
* Interrupt vector configuration
*************************************************************************

	ORG	$FFE6

***********************************************
* For EEPROM:
*
*	FDB	SVC
*--------------------------
*
* For RAM and THRSim:
*
	FCB	$00,$DC

	ORG	$00DC
	JMP	SVC
***********************************************

	END