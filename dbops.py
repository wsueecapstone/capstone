import os
import glob
import eyed3
import sqlite3

def init_mp3():
    """
    Initialization routine.  Initialize a new database, load a
    config file (if used), and setup all GPIO pins and interrupts.
    """
    #init_db()
    #init_gpio()
    #init_lcd()
    #init_config()


def init_db():
    """
    Remove existing database if it exists and populate a new one with
    current files.  This may change to save the database and check for changes
    on next startup.
    """
    files = glob.glob('/home/pi/Music/*.mp3')

    if (os.path.isfile('Music.db')):
        os.remove('Music.db')

    conn = sqlite3.connect('Music.db')
    c = conn.cursor()
    c.execute("""
        CREATE TABLE Songs (
        SongId INTEGER PRIMARY KEY ASC,
        Title text,
        Artist text
        );
        """)
    key = 1

    for f in files:
        mp3 = eyed3.load(f)
        title = mp3.tag.title
        artist = mp3.tag.artist
        data = (key, title, artist)
        c.execute('INSERT INTO Songs VALUES (?,?,?)', data)
        key += 1

    conn.commit()
    conn.close()


def unicode_to_ascii(record):
    """
    Return a list of ASCII strings when passed a tuple containing Unicode
    strings.  
    """
    decoded_rec = []

    for field in record:
        if(type(field) == unicode):
            decoded_rec.append(field.encode('ascii'))
        else:
            decoded_rec.append(field)

    return decoded_rec


def search_song(search_str, mode):
    """
    Search the current song database and return the relevant results.  
    The `search_str` parameter should be the string to be searched against,
    and `mode` is an integer that specifies whether to search by title or 
    artist.
    """
    song_titles = []
    search_by = {1:'Title', 2:'Artist'}
    conn = sqlite3.connect('Music.db')
    cur = conn.cursor()
    search_str = '%' + search_str + '%'
    query = ("""
        SELECT * FROM Songs
        WHERE %s
        LIKE '%s'
        """ % (search_by[mode], search_str))

    cur.execute(query)

    # Eventually these will need to be printed to the LCD screen and linked to 
    # the actual files that they reference
    for row in cur.fetchall():
        song_titles.append(unicode_to_ascii(row)[1])

    return song_titles

#init_db()
