import os
import glob
import eyed3
import sqlite3
import string
import Adafruit_CharLCD as LCD
import pygame as p
import menu
import dbops
import mp3

js = None
lcd = None

def init_db():
    """
    Remove existing database if it exists and populate a new one with
    current files.  This may change to save the database and check for changes
    on next startup.
    """
    files = glob.glob('/home/pi/Music/*.mp3')

    if (os.path.isfile('Music.db')):
        os.remove('Music.db')

    conn = sqlite3.connect('Music.db')
    c = conn.cursor()
    c.execute("""
        CREATE TABLE Songs (
        SongId INTEGER PRIMARY KEY ASC,
        Title text,
        Artist text
        );
        """)
    key = 1

    for f in files:
        mp3 = eyed3.load(f)
        title = mp3.tag.title
        artist = mp3.tag.artist
        data = (key, title, artist)
        c.execute('INSERT INTO Songs VALUES (?,?,?)', data)
        key += 1

    conn.commit()
    conn.close()


def init_joystick():
    p.init()
    j = p.joystick.Joystick(0)
    j.init()

    return j


def init_lcd():
    rs = 5
    en = 6
    d4 = 25
    d5 = 24
    d6 = 23
    d7 = 18
    cols = 16
    rows = 2

    screen = LCD.Adafruit_CharLCD(rs, en, d4, d5, d6, d7, cols, rows)

    return screen

def init_mp3():
    """
    Initialization routine.  Initialize a new database, the LCD screen, and 
    the joystick
    """
    init_db()
    init_lcd()
    init_joystick()

def main():
    #init_mp3()
    init_db()
    lcd = init_lcd()
    js = init_joystick()
    songs = dbops.search_song('', 1)    # Get list of all songs
    mp3.setup(js, songs, 0)
    song_to_play = menu.main_menu(lcd, js, songs)
    mp3.play(song_to_play)

if __name__ == "__main__":
    main()
