import string
import time
import pygame as p
import dbops
import Adafruit_CharLCD as LCD

def search_menu(lcd, joystick):
    left_edge = 0
    right_edge = left_edge + 15
    search_str = ''
    alphabet = string.ascii_uppercase + '<*' # `<*` for backspace and done
    alpha_pos = 0 # for position in alphabet row
    str_pos = 0 # for position in the built string
    bs_pos = len(alphabet) - 2  # for position of backspace
    done_pos = len(alphabet) - 1 # for position of done character
    str_row = 0
    alpha_row = 1
    done = False

    # Setup the environment
    p.event.set_allowed(None)
    p.event.set_allowed([p.JOYBUTTONDOWN, p.JOYHATMOTION])
    p.event.clear() # clear any unwanted events from the queue
    lcd.clear()
    lcd.set_cursor(alpha_pos, alpha_row) # set cursor to first column, second row
    lcd.blink(True)
    lcd.message(alphabet)
    lcd.set_cursor(alpha_pos, alpha_row)

    while not done:
        e = p.event.wait()

        if(e.type == 10):
            if(e.dict['button'] != 1):
                continue

            if(alpha_pos == bs_pos):
                if(str_pos == 0):
                    continue
                else:
                    search_str = search_str[:-1]
                    str_pos = str_pos - 1
                    lcd.set_cursor(str_pos, str_row)
                    lcd.message(' ')

                    if(str_pos < 0):
                        str_pos = 0

                    lcd.set_cursor(alpha_pos, alpha_row)

            elif(alpha_pos == done_pos):
                done = True

            else:
                search_str = search_str + alphabet[alpha_pos]
                lcd.set_cursor(str_pos, str_row)
                lcd.message(alphabet[alpha_pos])
                str_pos = str_pos + 1
                lcd.set_cursor(alpha_pos, alpha_row)

        elif(e.type == 9):
            if(e.dict['value'] == (-1, 0)):  # left
                if(alpha_pos == 0):
                    continue

                if(alpha_pos == left_edge):
                    lcd.move_right()
                    left_edge = left_edge - 1
                    right_edge = left_edge + 15

                alpha_pos = alpha_pos - 1

            elif(e.dict['value'] == (1, 0)):    # right
                if(alpha_pos == done_pos):
                    continue

                if(alpha_pos == right_edge):
                    lcd.move_left()
                    right_edge = right_edge + 1
                    left_edge = right_edge - 15

                alpha_pos = alpha_pos + 1

            lcd.set_cursor(alpha_pos, alpha_row)

        p.event.clear() # clear event queue of unwanted events

    lcd.clear()
    return search_str


def main_menu(lcd, joystick, song_list):
    rows = len(song_list)

    if(rows == 0):
        lcd.clear()
        lcd.message("No songs found")
        time.sleep(4)
        search_str = search_menu(lcd, joystick)
        songs = dbops.search_song(search_str, 1)
        return main_menu(lcd, joystick, songs)
        
    current_row = 0
    top = 0
    bottom = top + 1
    done = False

    # Setup the environment
    p.event.set_allowed(None)
    p.event.set_allowed([p.JOYBUTTONDOWN, p.JOYHATMOTION])
    p.event.clear() # clear any unwanted events from the queue
    lcd.clear()
    lcd.set_cursor(0, 0) # set cursor to first column, first row
    lcd.blink(True)

    if(rows > 1):
        lcd.message(song_list[top] + '\n' + song_list[bottom])
    else:
        lcd.message(song_list[top])

    lcd.set_cursor(0, 0)

    while not done:
        e = p.event.wait()

        if(e.type == 10):
            if(e.dict['button'] == 1):
                done = True

            elif(e.dict['button'] == 3):
                search_str = search_menu(lcd, joystick)
                songs = dbops.search_song(search_str, 1)
                return main_menu(lcd, joystick, songs)

        elif(e.type == 9):
            if(e.dict['value'] == (0, 1)):  # up
                if(current_row == top):
                    if(top == 0):
                        continue

                    else:
                        top = top - 1
                        bottom = top + 1
                        #lcd.set_cursor(0, 0)
                        lcd.clear()
                        lcd.message(song_list[top] + '\n' + song_list[bottom])
                        lcd.set_cursor(0, 0)
                        current_row = top

                else:
                    current_row = current_row - 1
                    lcd.set_cursor(0, 0)

            elif(e.dict['value'] == (0, -1)):   # down
                if(current_row == bottom):
                    if(bottom == rows - 1):
                        continue

                    else:
                        bottom = bottom + 1
                        top = bottom - 1
                        #lcd.set_cursor(0, 0)
                        lcd.clear()
                        lcd.message(song_list[top] + '\n' + song_list[bottom])
                        lcd.set_cursor(0, 1)
                        current_row = bottom

                else:
                    current_row = current_row + 1
                    lcd.set_cursor(0, 1)
        
        else:
            continue

    lcd.clear()

    return song_list[current_row]


def main():
    p.init()
    js = p.joystick.Joystick(0)
    js.init()
    lcd = LCD.Adafruit_CharLCD(5, 6, 25, 24, 23, 18, 16, 2)
    dbops.init_db()
    search = search_menu(lcd, js)
    songs = dbops.search_song(search, 1)
    
    ss = main_menu(lcd, js, songs)

    print(ss)
