import time
import sys
import spidev
import RPi.GPIO as GPIO
import binascii
import pygame
#from enum import Enum

#Constants
DREQ = 22  #GPIO PIN 15 TO MP3_DREQ
BSYNC = 27 #GPIO PIN 11 TO MP3_BSYNC
INIT_CLOCK_SPEED_HZ=1000000 #SPI clock rate speed when VS1063 is initializing
DATA_XFER_SPEED_HZ=4000000 #SPI clock rate speed after VS1063 has initialized, use this speed when sending MP3 data
DUMMY_BYTE=0XFF #Dummy Transfer Byte
VS1063_CLOCK=[0x60,0x00] #Clock multiplier settings for VS1063
PLAY_MODE=False #Set False to pause.

BUTTON_PLAY_PAUSE=1 #Joystick button values mapped to human readable format.
BUTTON_PREV_TRACK=4
BUTTON_NEXT_TRACK=5
BUTTON_REWIND=6
BUTTON_FAST_FORWARD=7

#Register Addresses on the VS1063
SCI_MODE=0x00
SCI_STATUS=0x01
SCI_BASS=0x02
SCI_CLOCKF=0x03
SCI_DECODE_TIME=0x04
SCI_AUDATA=0x05
SCI_WRAM=0x06
SCI_WRAMADDR=0X07
SCI_HDAT0=0x08
SCI_HDAT1=0x09
SCI_AIADDR=0x0A
SCI_VOL=0x0B
SCI_AICTRL0=0x0C
SCI_AICTRL1=0x0D
SCI_AICTRL2=0x0E
SCI_AICTRL3=0x0F
SCI_WRITECMD=0X02
SCI_READCMD=0X03

#Variables

#Mode = Enum('PLAYING', 'PAUSED', 'STOPPED')
#currentMode = Mode.STOPPED

file = None #Object that will open MP3 file
joystick = None #To catch button  presses
track_list = None #Holds list of tracks that are to be played
current_song = None #Holds index that corresponds to tracklist
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz=(INIT_CLOCK_SPEED_HZ)

#Functions
def writeRegister(address, dataHi, dataLo): #Write to VS1063's memory registers
    global spi
    while(not GPIO.input(DREQ)): #WAIT FOR DREQ TO GO HIGH
        pass

    spi.writebytes([SCI_WRITECMD, address, dataHi, dataLo]) #Send data

def readRegister(address): #Read from VS1063's memory registers
    global spi
    while(not GPIO.input(DREQ)):
        pass

    spi.writebytes([SCI_READCMD, address])
    resp = spi.readbytes(2) #Each read command returns two bytes

    return resp

def setVolume(vol): 
    writeRegister(SCI_VOL, vol, vol) #vol should be 0x00-0xFF

def debugOut(msg):
    if(__name__ == "__main__"):
        print msg

#Initialize SPI and attempt to connect to VS1063
def setup(Joysticks, Tracklist, index):
    global spi
    global track_list
    global current_song
    debugOut("Connecting to VS1063...")
    #spi = spidev.SpiDev()
    #spi.open(0, 0)
    #spi.max_speed_hz=(INIT_CLOCK_SPEED_HZ)
    spi.xfer2([DUMMY_BYTE])


    GPIO.setmode(GPIO.BCM) #Use GPIO number that's on the board.
    GPIO.setup(DREQ, GPIO.IN)
    GPIO.setup(BSYNC, GPIO.OUT)
    GPIO.output(BSYNC, GPIO.HIGH) #Set BSYNC high to disable Data Mode

    joystick = Joysticks
    track_list = Tracklist
    current_song = index

#Print Config Info from VS1063  
def printInfo():
    try:
        #Attempt to read Mode, Status, and Clock registers. 
        print "SCI_MODE: " 
        for x in readRegister(SCI_MODE): print '{0:08b}'.format(x)
        
        print "SCI_STATUS response: "
        for x in readRegister(SCI_STATUS): print '{0:08b}'.format(x)

        print "SCI_CLOCKF response: "
        for x in readRegister(SCI_CLOCKF): print '{0:08b}'.format(x)
    
    except:
        print "Error: ", sys.exc_info()[1]  
        
#Set VS1063 Clock Multiplier
def setClock(clockByteHi, clockByteLo):
    global spi
    try:
        #Attempt to set VS1063 clock multiplier to 3.0
        debugOut("Attempting to set VS1063 Clock...")
        writeRegister(SCI_CLOCKF, clockByteHi, clockByteLo)

        #VS1063 clock runs at clock multiplier 1.0 for 1200 cycles after writing to that register. Night Night.
        time.sleep(0.25)

        #Print new clock register
        debugOut("New SCI_CLOCKF response: ")
        for x in readRegister(SCI_CLOCKF): debugOut('{0:08b}'.format(x))
        
        #Increase SPI max speed locally also
        spi.max_speed_hz=(DATA_XFER_SPEED_HZ)
    
    except:
        print "Error: ", sys.exc_info()[1]
        
#You guessed it.
def play(filePath):
    global spi
    global file
    global PLAY_MODE
    setClock(0x60, 0x00)
    
    try:
        PLAY_MODE = True
        
        debugOut("Loading MP3 into memory...")
        
        #Open Song from reading and grab the first 32 bytes.
        file = open("/home/pi/Music/" + filePath + ".mp3", 'rb')
        mp3bytes = bytearray(file.read(32))

        debugOut("Done.")
        debugOut("Playing MP3: "+filePath)

        button_check = 0  #Integer to delay checks for button presses.
    
        while(len(mp3bytes) >= 32): #Loop until EoF

            #DREQ must be checked after sending 32 bytes
            while((not GPIO.input(DREQ)) or (not PLAY_MODE) ): #Wait if buffer is full, DREQ will be high, or if we enabled pause.
                pass
                if(pygame.event.peek(pygame.JOYBUTTONDOWN)):
                   processButtonPress()
        
            GPIO.output(BSYNC, GPIO.LOW) #Go into data mode
            for x in mp3bytes: spi.writebytes([x]) #Send bytes

            if(pygame.event.peek(pygame.JOYBUTTONDOWN) and (button_check % 4000 == 0)): #Check for button presses
                processButtonPress()
            
            GPIO.output(BSYNC, GPIO.HIGH) #Come out of data mode
            mp3bytes = bytearray(file.read(32)) #Grab next chunk of song
            button_check += 32

        #end while
        
        #Send 2048 zero bytes after the song is finished, probably to flush the buffer on the VS1063.
        GPIO.output(BSYNC, GPIO.LOW)
        for x in xrange(0, 2047): spi.writebytes([x])
        GPIO.output(BSYNC, GPIO.HIGH)
        PLAY_MODE = False
        debugOut("Playing finished.")

    except KeyboardInterrupt:
        spi.close()
        file.close()
        GPIO.cleanup()
    #end try

def processButtonPress():
    global current_song
    global file
    global track_list
    global PLAY_MODE
    e = pygame.event.get(pygame.JOYBUTTONDOWN)
    if(e[0].button == BUTTON_PLAY_PAUSE):
        PLAY_MODE = not PLAY_MODE

    elif(e[0].button == BUTTON_NEXT_TRACK):
        current_song = current_song + 1
        file.close()
        play(track_list[current_song])

    elif(e[0].button == BUTTON_PREV_TRACK):
        current_song = current_song - 1
        file.close()
        play(track_list[current_song])

    elif(e[0].button == BUTTON_FAST_FORWARD):
        if(PLAY_MODE):
            file.seek(64000,1)

    elif(e[0].button == BUTTON_REWIND):
        if(PLAY_MODE):
            file.seek(-64000,1)

    pygame.event.clear()

if(__name__ == "__main__"):
    pygame.init()
    j = pygame.joystick.Joystick(0)
    j.init()
    setup(j, [], 0)
    printInfo()
    play('Counting Stars')
        
    
