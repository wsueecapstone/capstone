#Debug script that prints out all bytes from SPI port.

import time
import sys
import spidev
import RPi.GPIO as GPIO
import binascii

#Init
print "Connecting to VS1063..."
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz=(1000000)


try:
#	file = open('/home/pi/Music/Letters.mp3','rb')

	while True:
#		byte = ord(file.read(1))
		spi.writebytes([0x03,0x00])
		resp = spi.readbytes(2)
		for n in resp: print '0x{0:02x}'.format(n)
		time.sleep(1)

	#end while

except KeyboardInterrupt:
	spi.close()
#	file.close()
#end try

